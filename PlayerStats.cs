/*Cameron Deao
 * CST-227
 * James Shinevar
 * 7/14/2019
 * Repo: https://bitbucket.org/cdeao/cst-227-milestone-6/src/master/ */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using Cameron_Deao_Milestone_1;

namespace Cameron_Deao_Milestone_1
{
    //PlayerStats implements the IComparable interface.
    public class PlayerStats : IComparable<PlayerStats>
    {
        //Variables that will be used throughout the file.
        public string playerName;
        public string levelPlayed;
        public double timePlayed;
        public bool gameCompleted;
        public static List<PlayerStats> leaderBoard = new List<PlayerStats>();
        public static List<PlayerStats> writeList = new List<PlayerStats>();

        //Compare method
        public int CompareTo(PlayerStats other)
        {
            return this.timePlayed.CompareTo(other.timePlayed);
        }

        //Returning the PlayerStats objects with the appropriate variables.
        public override string ToString()
        {
            return this.playerName + "," + this.levelPlayed + "," + this.timePlayed.ToString() + "," + this.gameCompleted;
        }

        //Method used to read data from the specified file.
        public static void ReadData(string difficulty)
        {
            //Checking if the file exists at the path.
            if (File.Exists(@"C:\Users\Public\TestFolder\MinesweeperPlayerStats.txt"))
            {
                List<PlayerStats> players = new List<PlayerStats>();
                //Creating the StreamReader to read the file.
                StreamReader input = new StreamReader(@"C:\Users\Public\TestFolder\MinesweeperPlayerStats.txt");
                String line;
                //Char array of characters to be removed from the file.
                char[] removedChars = { ' ', ',', '.', ':', '\t' };
                //Reading in the data into a specified list.
                while ((line = input.ReadLine()) != null)
                {
                    String[] token = line.Split(removedChars);
                    players.Add(new PlayerStats() { playerName = token[0], levelPlayed = token[1], timePlayed = Convert.ToDouble(token[2]) });
                }
                //Closing the StreamReader.
                input.Close();

                //Each if-statement below checks against the difficulty level that was
                //passed into the method and creates a list that is set to the 
                //LeaderBoard list for display. Only the top five scores are sent to
                //be displayed.

                //Checking what difficulty was selected.
                if (difficulty == "Easy")
                {
                    //A list is created for the elements that contain that difficutly level.
                    List<PlayerStats> easyList = new List<PlayerStats>();
                    foreach (var element in players)
                    {
                        //Checking each elements levelPlayed against the value
                        //passed into the method.
                        if (element.levelPlayed == difficulty)
                        {
                            easyList.Add(element);
                        }
                    }
                    //Checks if the list is higher than a count of 5 and removes
                    //the highest scored value from the list.
                    while (easyList.Count > 5)
                    {
                        //Linq statement to find the max value within the list
                        //and set the double variable to that value.
                        double maxScore = easyList.Max(s => s.timePlayed);
                        //Iterating through the list and finding the element with 
                        //the max score and removing it from the list.
                        for (int i = 0; i < easyList.Count; i++)
                        {
                            if (easyList[i].timePlayed >= maxScore)
                            {
                                easyList.RemoveAt(i);
                            }
                        }
                    }
                    //Setting the LeaderBoard list to the appropriate list.
                    LeaderBoard.playerBoard = easyList;
                }
                if (difficulty == "Medium")
                {
                    List<PlayerStats> mediumList = new List<PlayerStats>();
                    foreach (var element in players)
                    {
                        if (element.levelPlayed == difficulty)
                        {
                            mediumList.Add(element);
                        }
                    }
                    while (mediumList.Count > 5)
                    {
                        double maxScore = mediumList.Max(s => s.timePlayed);
                        for (int i = 0; i < mediumList.Count; i++)
                        {
                            if (mediumList[i].timePlayed >= maxScore)
                            {
                                mediumList.RemoveAt(i);
                            }
                        }
                    }
                    LeaderBoard.playerBoard = mediumList;
                }
                if (difficulty == "Hard")
                {
                    List<PlayerStats> hardList = new List<PlayerStats>();
                    foreach (var element in players)
                    {
                        if (element.levelPlayed == difficulty)
                        {
                            hardList.Add(element);
                        }
                    }
                    while (hardList.Count > 5)
                    {
                        double maxScore = hardList.Max(s => s.timePlayed);
                        for (int i = 0; i < hardList.Count; i++)
                        {
                            if (hardList[i].timePlayed >= maxScore)
                            {
                                hardList.RemoveAt(i);
                            }
                        }
                    }
                    LeaderBoard.playerBoard = hardList;
                }

                foreach (var element in players)
                {
                    writeList.Add(element);
                }
            }
            //Fires if the file does not exist at the specified path.
            else
            {
                Console.WriteLine("FILE DOES NOT EXIST");
            }
        }
        //Method of adding a new player to the master list.
        public static void AddPlayer(PlayerStats player)
        {
            writeList.Add(player);
        }

        //Method used to write the data to the file.
        public static void WriteData()
        {
            //Calling the sort for the master list.
            writeList.Sort();
            //Using a StreamWriter to write the file.
            using (System.IO.StreamWriter file =
               new System.IO.StreamWriter(@"C:\Users\Public\TestFolder\MinesweeperPlayerStats.txt"))
            {
                //Writing each element into a single line within the file that was specified.
                foreach (var element in writeList)
                {
                    file.WriteLine(element.playerName + "," + element.levelPlayed + "," + element.timePlayed + "," + element.gameCompleted);
                }
            }
        }
    }
}