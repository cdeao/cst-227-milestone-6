/*Cameron Deao
 * CST-227
 * James Shinevar
 * 7/14/2019
 * Repo: https://bitbucket.org/cdeao/cst-227-milestone-6/src/master/ */

using Cameron_Deao_Milestone_1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Cameron_Deao_Milestone_2
{
    class Program
    {
        //Updated the main method to display the proper windows in the proper
        //order of appearance.
        static void Main()
        {
            ChoosingDifficulty choosingDifficulty = new ChoosingDifficulty();
            Application.Run(choosingDifficulty);
            int size = choosingDifficulty.GridSize();
            string difficulty = choosingDifficulty.DifficultySelected();
            string userName = choosingDifficulty.UserName();
            Grid newGrid = new Grid();
            LeaderBoard leader = new LeaderBoard();
            newGrid.SetDifficulty(difficulty);
            newGrid.SetUsername(userName);
            newGrid.SetupButtons(size);
            Application.Run(newGrid);
            PlayerStats.ReadData(difficulty);
            PlayerStats.WriteData();
            PlayerStats.ReadData(difficulty);
            leader.SetLeaderboard();
            Application.Run(leader);
        }
    }
}